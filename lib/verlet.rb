# frozen_string_literal: true

require_relative "verlet/version"

module Verlet
  class Error < StandardError; end
  # Your code goes here...
end
